#include "c_types.h"
#include "esp_common.h"
#include "gpio.h"

#include "io_gpio.h"

////Built-In Function/////
void gpio_output_conf(uint32 set_mask, uint32 clear_mask, uint32 enable_mask, uint32 disable_mask)
{
    GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, set_mask);
    GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, clear_mask);
    GPIO_REG_WRITE(GPIO_ENABLE_W1TS_ADDRESS, enable_mask);
    GPIO_REG_WRITE(GPIO_ENABLE_W1TC_ADDRESS, disable_mask);
}
//*******************************//
////Built-In Function/////
void gpio16_output_conf(void)
{
    WRITE_PERI_REG(PAD_XPD_DCDC_CONF,
                   (READ_PERI_REG(PAD_XPD_DCDC_CONF) & 0xffffffbc) | (uint32)0x1); 	// mux configuration for XPD_DCDC to output rtc_gpio0

    WRITE_PERI_REG(RTC_GPIO_CONF,
                   (READ_PERI_REG(RTC_GPIO_CONF) & (uint32)0xfffffffe) | (uint32)0x0);	//mux configuration for out enable

    WRITE_PERI_REG(RTC_GPIO_ENABLE,
                   (READ_PERI_REG(RTC_GPIO_ENABLE) & (uint32)0xfffffffe) | (uint32)0x1);	//out enable
}
//*******************************//
////Built-In Function/////
void gpio16_output_set(uint8 value)
{
    WRITE_PERI_REG(RTC_GPIO_OUT,
                   (READ_PERI_REG(RTC_GPIO_OUT) & (uint32)0xfffffffe) | (uint32)(value & 1));
}
//*******************************//
////Built-In Function/////
void gpio16_input_conf(void)
{
    WRITE_PERI_REG(PAD_XPD_DCDC_CONF,
                   (READ_PERI_REG(PAD_XPD_DCDC_CONF) & 0xffffffbc) | (uint32)0x1); 	// mux configuration for XPD_DCDC and rtc_gpio0 connection

    WRITE_PERI_REG(RTC_GPIO_CONF,
                   (READ_PERI_REG(RTC_GPIO_CONF) & (uint32)0xfffffffe) | (uint32)0x0);	//mux configuration for out enable

    WRITE_PERI_REG(RTC_GPIO_ENABLE,
                   READ_PERI_REG(RTC_GPIO_ENABLE) & (uint32)0xfffffffe);	//out disable
}
//*******************************//

//Lib Function//
inventOne_Bool validGPIOPin(unsigned int pin)
{
    inventOne_Bool returnVal = inventOne_FALSE;
    if(0 <= pin && pin <= 16)
    {
        returnVal = inventOne_TRUE;
    }
    return returnVal;
}

inventOne_Bool io_getPinNameAndFunction(unsigned int pin, uint32_t *gpio_name, uint32_t *gpio_function)
{
    inventOne_Bool returnVal = inventOne_FALSE;
    switch (pin)
    {
        case 0:
            *gpio_name = PERIPHS_IO_MUX_GPIO0_U;
            *gpio_function = FUNC_GPIO0;
            returnVal = inventOne_TRUE;
            break;
        case 1:
            *gpio_name = PERIPHS_IO_MUX_U0TXD_U;
            *gpio_function = FUNC_GPIO1;
            returnVal = inventOne_TRUE;
            break;
        case 2:
            *gpio_name = PERIPHS_IO_MUX_GPIO2_U;
            *gpio_function = FUNC_GPIO2;
            returnVal = inventOne_TRUE;
            break;
        case 3:
            *gpio_name = PERIPHS_IO_MUX_U0RXD_U;
            *gpio_function = FUNC_GPIO3;
            returnVal = inventOne_TRUE;
            break;
        case 4:
            *gpio_name = PERIPHS_IO_MUX_GPIO4_U;
            *gpio_function = FUNC_GPIO4;
            returnVal = inventOne_TRUE;
            break;
        case 5:
            *gpio_name = PERIPHS_IO_MUX_GPIO5_U;
            *gpio_function = FUNC_GPIO5;
            returnVal = inventOne_TRUE;
            break;
        case 9:
            *gpio_name = PERIPHS_IO_MUX_SD_DATA2_U;
            *gpio_function = FUNC_GPIO9;
            returnVal = inventOne_TRUE;
            break;
        case 10:
            *gpio_name = PERIPHS_IO_MUX_SD_DATA3_U;
            *gpio_function = FUNC_GPIO10;
            returnVal = inventOne_TRUE;
            break;
        case 12:
            *gpio_name = PERIPHS_IO_MUX_MTDI_U;
            *gpio_function = FUNC_GPIO12;
            returnVal = inventOne_TRUE;
            break;
        case 13:
            *gpio_name = PERIPHS_IO_MUX_MTCK_U;
            *gpio_function = FUNC_GPIO13;
            returnVal = inventOne_TRUE;
            break;
        case 14:
            *gpio_name = PERIPHS_IO_MUX_MTMS_U;
            *gpio_function = FUNC_GPIO14;
            returnVal = inventOne_TRUE;
            break;
        case 15:
            *gpio_name = PERIPHS_IO_MUX_MTDO_U;
            *gpio_function = FUNC_GPIO15;
            returnVal = inventOne_TRUE;
            break;
        default:
            returnVal = inventOne_FALSE;
            break;
    }
    return returnVal;
}

inventOne_Bool io_pinMode(unsigned int pin, inventOne_PinMode pinMode)
{
    uint32_t gpio_name;
    uint32_t gpio_function;
    inventOne_Bool returnVal = inventOne_FALSE;
    if(validGPIOPin(pin))
    {
        if(pin != 16)
        {
            io_getPinNameAndFunction(pin, &gpio_name, &gpio_function);
            if(pinMode == inventOne_OUTPUT_Mode)
            {
                //pinMode as output
                PIN_FUNC_SELECT(gpio_name, gpio_function);
                GPIO_AS_OUTPUT(BIT(GPIO_ID_PIN(pin)));
                returnVal = inventOne_TRUE;
            }
            else if(pinMode == inventOne_INPUT_Mode)
            {
                //pinMode as input
                PIN_FUNC_SELECT(gpio_name, gpio_function);
                GPIO_AS_INPUT(BIT(GPIO_ID_PIN(pin)));
                returnVal = inventOne_TRUE;
            }
        }
        else if(pin == 16)
        {
            //special pin 16
            if(pinMode == inventOne_OUTPUT_Mode)
            {
                //pin 16 as output
                gpio16_output_conf();
                returnVal = inventOne_TRUE;
            }
            else if(pinMode == inventOne_INPUT_Mode)
            {
                //pin 16 as input
                gpio16_input_conf();
                returnVal = inventOne_TRUE;
            }
        }
    }
    return returnVal;
} 

inventOne_Bool io_digitalWrite(unsigned int pin, inventOne_State state)
{
    inventOne_Bool returnVal = inventOne_FALSE;
    if(validGPIOPin(pin))
    {
        if(state == inventOne_HIGH)
        {
            if(pin != 16)
            {
                GPIO_OUTPUT(BIT(GPIO_ID_PIN(pin)), state);
                returnVal = inventOne_TRUE;
            }
            else
            {
                gpio16_output_set(state);
                returnVal = inventOne_TRUE;
            }
        }
        else if(state == inventOne_LOW)
        {
            if(pin != 16)
            {
                GPIO_OUTPUT(BIT(GPIO_ID_PIN(pin)), state);
                returnVal = inventOne_TRUE;
            }
            else
            {
                gpio16_output_set(state);
                returnVal = inventOne_TRUE;
            }
        }
    }
    return returnVal;
}

uint32 io_digitalRead(unsigned int pin)
{
    if(validGPIOPin(pin))
    {
        if(pin != 16)
        {
            return GPIO_INPUT_GET(GPIO_ID_PIN(pin));
        }
        else
        {
            return gpio_input_get();
        }
    }    
}