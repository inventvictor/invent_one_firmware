/*
 * ESPRSSIF MIT License
 *
 * Copyright (c) 2015 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on ESPRESSIF SYSTEMS ESP8266 only, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


/*** Todo
* Victor Shoaga work on SPIFFS setup
* Tobi Ogunbayo work on ds18b20 support
* Toba Adesanya work on LM35 support
* Tinuade Adeleke work on dht support
*
*
*
* Please guys dont forget.... create your own branch and then push to the dev_branch
* do something like...git push origin tinuade_branch (if the branch I created was tinuade_branch)
*/

#include "esp_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/portmacro.h"
#include "io_gpio.h"
#include "nuart.h"
#include "dmsg.h"
#include "xmit.h"
#include "MQTTESP8266.h"
#include "MQTTClient.h"

#define STA_SSID    "DEVELOPMENT STUDIO"
#define STA_PASSWORD  "studiodev"
// testing mosquitto server
#define MQTT_HOST "192.168.0.120"
#define MQTT_PORT 1883
#define MQTT_USER ""
#define MQTT_PASS ""

xSemaphoreHandle wifi_alive;
xSemaphoreHandle led_received;
xQueueHandle publish_queue;

#define PUB_MSG_LEN 16
#define SUB_MSG_LEN 500

/******************************************************************************
 *
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABCCC
 *                A : rf cal
 *                B : rf init data
 *                C : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
 *
*******************************************************************************/
uint32 user_rf_cal_sector_set(void)
{
    flash_size_map size_map = system_get_flash_size_map();
    uint32 rf_cal_sec = 0;

    switch (size_map) {
        case FLASH_SIZE_4M_MAP_256_256:
            rf_cal_sec = 128 - 5;
            break;

        case FLASH_SIZE_8M_MAP_512_512:
            rf_cal_sec = 256 - 5;
            break;

        case FLASH_SIZE_16M_MAP_512_512:
        case FLASH_SIZE_16M_MAP_1024_1024:
            rf_cal_sec = 512 - 5;
            break;

        case FLASH_SIZE_32M_MAP_512_512:
        case FLASH_SIZE_32M_MAP_1024_1024:
            rf_cal_sec = 1024 - 5;
            break;

        default:
            rf_cal_sec = 0;
            break;
    }

    return rf_cal_sec;
}

LOCAL const char * ICACHE_FLASH_ATTR get_my_id(void)
{
    // Use MAC address for Station as unique ID
    static char my_id[13];
    static bool my_id_done = false;
    int8_t i;
    
    uint8_t x;
    if (my_id_done)
        return my_id;
    if (!wifi_get_macaddr(STATION_IF, my_id))
        return NULL;
    for (i = 5; i >= 0; --i)
    {
        x = my_id[i] & 0x0F;
        if (x > 9) x += 7;
        my_id[i * 2 + 1] = x + '0';
        x = my_id[i] >> 4;
        if (x > 9) x += 7;
        my_id[i * 2] = x + '0';
    }
    my_id[12] = '\0';
    my_id_done = true;
    return my_id;
}

void ICACHE_FLASH_ATTR 
LEDBlinkTask (void *pvParameters)
{
    
    while(1)
    {
        xSemaphoreTake(led_received, portMAX_DELAY);
        while(1)
        {
            vTaskDelay(500/portTICK_RATE_MS);
            io_digitalWrite(5, inventOne_HIGH);
            printf("High");
            vTaskDelay (500/portTICK_RATE_MS);
            io_digitalWrite(5, inventOne_LOW);
            printf("Low");
        }
        
    }
}


void ICACHE_FLASH_ATTR 

io_setup(void)
{
    // uart_init_new;
    //UART_SetPrintPort(UART0);

    // printf("SDK version:%s\n", system_get_sdk_version());
    //  printf("Let the game begin now \n");
    //  printf("Thank you Lord, Victor Shoaga in the building :); \n");
    dmsg_init();
    dmsg_puts("Victor Shoaga");
    while(1)
    {
        io_pinMode(5, inventOne_OUTPUT_Mode);
        vTaskDelay(500/portTICK_RATE_MS);
        io_digitalWrite(5, inventOne_HIGH);
        dmsg_puts("High \r\n");
        vTaskDelay (500/portTICK_RATE_MS);
        io_digitalWrite(5, inventOne_LOW);
        dmsg_puts("Low \r\n");
    }
    
    // vSemaphoreCreateBinary(wifi_alive);
    // vSemaphoreCreateBinary(led_received);
    // publish_queue = xQueueCreate(3, PUB_MSG_LEN);
    // xSemaphoreTake(wifi_alive, 0);  // take the default semaph
    // xSemaphoreTake(led_received, 0);  // take the default semaph
}

/* Demonstrating sending something to MQTT broker
   In this task we simply queue up messages in publish_queue. The MQTT task will dequeue the
   message and sent.
 */
LOCAL void ICACHE_FLASH_ATTR beat_task(void * pvParameters)
{
    portTickType xLastWakeTime = xTaskGetTickCount();
    char msg[PUB_MSG_LEN];
    int count = 0;

    while (1)
    {
        vTaskDelayUntil(&xLastWakeTime, 5000 / portTICK_RATE_MS); // This is executed every 5000ms
        printf("beat\r\n");
        snprintf(msg, PUB_MSG_LEN, "Beat %d\r\n", count++);
        if (xQueueSend(publish_queue, (void *)msg, 0) == pdFALSE)
        {
            printf("Publish queue overflow.\r\n");
        }
    }
}

LOCAL void ICACHE_FLASH_ATTR wifi_task(void *pvParameters)
{
    uint8_t status;
    if (wifi_get_opmode() != STATION_MODE)
    {
        wifi_set_opmode(STATION_MODE);
        vTaskDelay(500 / portTICK_RATE_MS);
        system_restart();
    }
    while (1)
    {
        printf("WiFi: Connecting to WiFi\n");
        wifi_station_connect();
        struct station_config *config = (struct station_config *)zalloc(sizeof(struct station_config));
        sprintf(config->ssid, STA_SSID);
        sprintf(config->password, STA_PASSWORD);
        wifi_station_set_config(config);
        free(config);
        status = wifi_station_get_connect_status();
        int8_t retries = 30;
        while ((status != STATION_GOT_IP) && (retries > 0))
        {
            printf(".");
            status = wifi_station_get_connect_status();
            if (status == STATION_WRONG_PASSWORD)
            {
                printf("WiFi: Wrong password\n");
                break;
            }
            else if (status == STATION_NO_AP_FOUND)
            {
                printf("WiFi: AP not found\n");
                break;
            }
            else if (status == STATION_CONNECT_FAIL)
            {
                printf("WiFi: Connection failed\n");
                break;
            }
            vTaskDelay(500 / portTICK_RATE_MS);
            --retries;
        }
        if (status == STATION_GOT_IP)
        {
            printf("WiFi: Connected\n");
            vTaskDelay(500 / portTICK_RATE_MS);
        }
        while ((status = wifi_station_get_connect_status()) == STATION_GOT_IP)
        {
            xSemaphoreGive(wifi_alive);
            //printf("WiFi: Alive\n");
            vTaskDelay(500 / portTICK_RATE_MS);
        }
        printf("WiFi: Disconnected\n");
        wifi_station_disconnect();
        vTaskDelay(500 / portTICK_RATE_MS);
    }
}

LOCAL void ICACHE_FLASH_ATTR topic_received(MessageData* md)
{
    int i;
    char obtData[SUB_MSG_LEN];
    MQTTMessage* message = md->message;
    printf("Received: ");
    //printf((char*)message->payload);
    // for (i = 0; i < md->topic->lenstring.len; ++i)
    //     printf(md->topic->lenstring.data[i]);
    // printf(" = ");
    for (i = 0; i < (int)message->payloadlen; ++i)
    {
        obtData[i] = ((char*)message->payload)[i];
    }
    obtData[(int)message->payloadlen] = '\0';
    printf(obtData);
    if(strcmp(obtData, "LED") == 0)
    {
        xSemaphoreGive(led_received);
    }
        //printf(((char*)message->payload)[i]);
    printf("\r\n");
}

LOCAL void ICACHE_FLASH_ATTR mqtt_task(void *pvParameters)
{
    int ret;
    struct Network network;
    MQTTClient client = DefaultClient;
    char mqtt_client_id[20];
    unsigned char mqtt_buf[100];
    unsigned char mqtt_readbuf[100];
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;

    NewNetwork(&network);
    while (1)
    {
        // Wait until wifi is up
        xSemaphoreTake(wifi_alive, portMAX_DELAY);

        // Unique client ID
        strcpy(mqtt_client_id, "ESP-");
        strcat(mqtt_client_id, get_my_id());

        printf("(Re)connecting to MQTT server %s ... ", MQTT_HOST);
        ret = ConnectNetwork(&network, MQTT_HOST, MQTT_PORT);
        if (!ret)
        {
            printf("ok.\r\n");
            NewMQTTClient(&client, &network, 5000, mqtt_buf, 100, mqtt_readbuf, 100);
            data.willFlag = 0;
            data.MQTTVersion = 3;
            data.clientID.cstring = mqtt_client_id;
            //data.username.cstring = MQTT_USER;
            //data.password.cstring = MQTT_PASS;
            data.keepAliveInterval = 10;
            data.cleansession = 0;
            printf("Send MQTT connect ...");
            ret = MQTTConnect(&client, &data);
            if (!ret)
            {
                printf("ok.\r\n");
                // Subscriptions
                MQTTSubscribe(&client, "/mytopic", QOS1, topic_received);
                // Empty the publish queue
                xQueueReset(publish_queue);
                while (1)
                {
                    // Publish all pending messages
                    char msg[PUB_MSG_LEN];
                    while (xQueueReceive(publish_queue, (void *)msg, 0) == pdTRUE)
                    {
                        msg[PUB_MSG_LEN - 1] = '\0';
                        MQTTMessage message;
                        message.payload = msg;
                        message.payloadlen = PUB_MSG_LEN;
                        message.dup = 0;
                        message.qos = QOS1;
                        message.retained = 0;
                        ret = MQTTPublish(&client, "beat", &message);
                        if (ret != SUCCESS)
                            break;
                    }
                    // Receiving / Ping
                    ret = MQTTYield(&client, 500);
                    if (ret == DISCONNECTED)
                    {
                        break;
                    }
                }
                printf("Connection broken, request restart\r\n");
            }
            else
            {
                printf("failed.\r\n");
            }
            DisconnectNetwork(&network);
        }
        else
        {
            printf("failed.\r\n");
        }
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    printf("MQTT task ended\r\n", ret);
    vTaskDelete(NULL);
}

/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR 
user_init(void)
{
    vSemaphoreCreateBinary(wifi_alive);
    vSemaphoreCreateBinary(led_received);
    publish_queue = xQueueCreate(3, PUB_MSG_LEN);
    xSemaphoreTake(wifi_alive, 0);  // take the default semaphore
    xSemaphoreTake(led_received, 0);  // take the default semaphore
    //os_printf("Hello world");
    //io_setup();
    //This task blinks the LED continuously
    xTaskCreate(beat_task, "beat", 256, NULL, tskIDLE_PRIORITY + 3, NULL);
    xTaskCreate(LEDBlinkTask, "Blink", 256, NULL, tskIDLE_PRIORITY + 2, NULL);
    xTaskCreate(mqtt_task, "mqtt", 1024, NULL, tskIDLE_PRIORITY + 2, NULL);
    xTaskCreate(wifi_task, "wifi", 256, NULL, tskIDLE_PRIORITY + 1, NULL);
}