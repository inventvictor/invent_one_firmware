
#ifndef __IO_GPIO_H__
#define __IO_GPIO_H__

typedef enum
{
    inventOne_INPUT_Mode = 1,
    inventOne_OUTPUT_Mode = 0
}inventOne_PinMode;

typedef enum
{
    inventOne_HIGH = 1,
    inventOne_LOW = 0
}inventOne_State;

typedef enum
{
    inventOne_TRUE = 1,
    inventOne_FALSE = 0
}inventOne_Bool;

inventOne_Bool io_pinMode(unsigned int pin, inventOne_PinMode pinMode);

inventOne_Bool io_digitalWrite(unsigned int pin, inventOne_State state);

uint32 io_digitalRead(unsigned int pin);

#endif
